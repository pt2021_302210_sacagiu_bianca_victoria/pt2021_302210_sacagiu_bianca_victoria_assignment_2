import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

public class View extends JFrame {

    private JPanel contentPane;
    private JButton startBtn;
    private JTextField nrOfQueuesTxt;
    private JTextField nrOfClientsTxt;
    private JTextField simTimeTxt;
    private JTextField minArrTimeTxt;
    private JTextField maxArrTimeTxt;
    private JTextField minServiceTimeTxt;
    private JTextField maxServiceTimeTxt;

    public View() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 401, 581);
        contentPane = new JPanel();
        contentPane.setBackground(new Color(153, 204, 204));
        contentPane.setForeground(new Color(102, 102, 153));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JLabel titleLbl = new JLabel("QUEUES SIMULATOR");
        titleLbl.setFont(new Font("Times New Roman", Font.BOLD, 25));
        titleLbl.setForeground(new Color(102, 102, 153));
        titleLbl.setBounds(51, 10, 348, 56);
        contentPane.add(titleLbl);

        JLabel qLbl = new JLabel("Number of queues:");
        qLbl.setFont(new Font("Times New Roman", Font.BOLD, 18));
        qLbl.setForeground(new Color(102, 102, 153));
        qLbl.setBounds(23, 112, 168, 32);
        contentPane.add(qLbl);

        JLabel cLbl = new JLabel("Number of clients:");
        cLbl.setForeground(new Color(102, 102, 153));
        cLbl.setFont(new Font("Times New Roman", Font.BOLD, 18));
        cLbl.setBounds(23, 164, 151, 13);
        contentPane.add(cLbl);

        JLabel simTimeLbl = new JLabel("Simulation time:");
        simTimeLbl.setFont(new Font("Times New Roman", Font.BOLD, 18));
        simTimeLbl.setForeground(new Color(102, 102, 153));
        simTimeLbl.setBounds(23, 202, 138, 26);
        contentPane.add(simTimeLbl);

        JLabel maxArrLbl = new JLabel("Max arrival time:");
        maxArrLbl.setFont(new Font("Times New Roman", Font.BOLD, 18));
        maxArrLbl.setForeground(new Color(102, 102, 153));
        maxArrLbl.setBounds(23, 291, 168, 26);
        contentPane.add(maxArrLbl);

        JLabel minArrLbl = new JLabel("Min arrival time:");
        minArrLbl.setForeground(new Color(102, 102, 153));
        minArrLbl.setFont(new Font("Times New Roman", Font.BOLD, 18));
        minArrLbl.setBounds(23, 245, 151, 26);
        contentPane.add(minArrLbl);

        JLabel minServLbl = new JLabel("Min service time:");
        minServLbl.setForeground(new Color(102, 102, 153));
        minServLbl.setFont(new Font("Times New Roman", Font.BOLD, 18));
        minServLbl.setBounds(23, 337, 138, 26);
        contentPane.add(minServLbl);

        JLabel maxServLbl = new JLabel("Max service time:");
        maxServLbl.setForeground(new Color(102, 102, 153));
        maxServLbl.setFont(new Font("Times New Roman", Font.BOLD, 18));
        maxServLbl.setBounds(23, 378, 168, 26);
        contentPane.add(maxServLbl);

        nrOfQueuesTxt = new JTextField();
        nrOfQueuesTxt.setFont(new Font("Times New Roman", Font.BOLD, 17));
        nrOfQueuesTxt.setBackground(new Color(204, 204, 255));
        nrOfQueuesTxt.setForeground(new Color(102, 102, 153));
        nrOfQueuesTxt.setBounds(184, 119, 151, 23);
        contentPane.add(nrOfQueuesTxt);
        nrOfQueuesTxt.setColumns(10);

        nrOfClientsTxt = new JTextField();
        nrOfClientsTxt.setFont(new Font("Times New Roman", Font.BOLD, 17));
        nrOfClientsTxt.setBackground(new Color(204, 204, 255));
        nrOfClientsTxt.setForeground(new Color(102, 102, 153));
        nrOfClientsTxt.setBounds(184, 161, 151, 23);
        contentPane.add(nrOfClientsTxt);
        nrOfClientsTxt.setColumns(10);

        simTimeTxt = new JTextField();
        simTimeTxt.setFont(new Font("Times New Roman", Font.BOLD, 17));
        simTimeTxt.setForeground(new Color(102, 102, 153));
        simTimeTxt.setBackground(new Color(204, 204, 255));
        simTimeTxt.setBounds(184, 206, 151, 23);
        contentPane.add(simTimeTxt);
        simTimeTxt.setColumns(10);

        minArrTimeTxt = new JTextField();
        minArrTimeTxt.setFont(new Font("Times New Roman", Font.BOLD, 17));
        minArrTimeTxt.setBackground(new Color(204, 204, 255));
        minArrTimeTxt.setForeground(new Color(102, 102, 153));
        minArrTimeTxt.setBounds(184, 251, 151, 23);
        contentPane.add(minArrTimeTxt);
        minArrTimeTxt.setColumns(10);

        maxArrTimeTxt = new JTextField();
        maxArrTimeTxt.setFont(new Font("Times New Roman", Font.BOLD, 17));
        maxArrTimeTxt.setForeground(new Color(102, 102, 153));
        maxArrTimeTxt.setBackground(new Color(204, 204, 255));
        maxArrTimeTxt.setBounds(184, 295, 151, 23);
        contentPane.add(maxArrTimeTxt);
        maxArrTimeTxt.setColumns(10);

        minServiceTimeTxt = new JTextField();
        minServiceTimeTxt.setFont(new Font("Times New Roman", Font.BOLD, 17));
        minServiceTimeTxt.setForeground(new Color(102, 102, 153));
        minServiceTimeTxt.setBackground(new Color(204, 204, 255));
        minServiceTimeTxt.setBounds(184, 340, 151, 23);
        contentPane.add(minServiceTimeTxt);
        minServiceTimeTxt.setColumns(10);

        maxServiceTimeTxt = new JTextField();
        maxServiceTimeTxt.setFont(new Font("Times New Roman", Font.BOLD, 17));
        maxServiceTimeTxt.setBackground(new Color(204, 204, 255));
        maxServiceTimeTxt.setForeground(new Color(102, 102, 153));
        maxServiceTimeTxt.setBounds(184, 379, 151, 23);
        contentPane.add(maxServiceTimeTxt);
        maxServiceTimeTxt.setColumns(10);

        startBtn = new JButton("START");
        startBtn.setFont(new Font("Times New Roman", Font.BOLD, 21));
        startBtn.setForeground(new Color(102, 102, 153));
        //startBtn.setBackground(new Color(204, 204, 255));
        startBtn.setBounds(122, 452, 127, 32);
        contentPane.add(startBtn);
    }
    public JButton getSBtn()
    {
        return this.startBtn;
    }
    public String getNrOfQueues()
    {
        return this.nrOfQueuesTxt.getText();
    }
    public String getNrOfClients()
    {
        return this.nrOfClientsTxt.getText();
    }
    public String getSimTime()
    {
        return this.simTimeTxt.getText();
    }
    public String getMaxServiceTime()
    {
        return this.maxServiceTimeTxt.getText();
    }
    public String getMinServiceTime()
    {
        return this.minServiceTimeTxt.getText();
    }
    public String getMaxArrivalTime()
    {
        return this.maxArrTimeTxt.getText();
    }
    public String getMinArrivalTime()
    {
        return this.minArrTimeTxt.getText();
    }

}