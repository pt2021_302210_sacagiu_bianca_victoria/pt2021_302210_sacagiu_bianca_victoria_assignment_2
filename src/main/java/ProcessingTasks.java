import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class ProcessingTasks {
    public static void add(BlockingQueue<Task> t, Task task, AtomicInteger waiting){
        t.add(task);
        waiting.addAndGet(task.getTimpDeProcesare());
    }

    public static void generateRandomTasks(List<Task> clientiGenerati, int numarDeClienti, int timpMaximDeSosire, int timpMinimDeSosire,
                                           int timpMaximDeProcesare, int timpMinimDeProcesare){

        for(int i = 0; i < numarDeClienti; i ++){
            clientiGenerati.add(new Task(ProcessingTasks.generareNrRandom(timpMaximDeSosire, timpMinimDeSosire),
                    ProcessingTasks.generareNrRandom(timpMaximDeProcesare, timpMinimDeProcesare)));
        }
        ProcessingTasks.ordonareDupaTimpSosire(clientiGenerati);
    }

    public static int generareNrRandom(int boundMax, int boundMin){
        Random rand = new Random();
        return rand.nextInt(boundMax - boundMin) + boundMin;
    }

    public static void ordonareDupaTimpSosire(List<Task> clientiGenerati){
        for(int i = 0 ; i < clientiGenerati.size() - 1; i ++){
            for(int j = i + 1; j < clientiGenerati.size(); j ++) {
                if (clientiGenerati.get(i).getTimpDeSosire() > clientiGenerati.get(j).getTimpDeSosire()){
                    Task aux = clientiGenerati.get(i);
                    clientiGenerati.set(i, clientiGenerati.get(j));
                    clientiGenerati.set(j, aux);
                }
            }
        }
    }

    public static void utilManager1(List<Task> clientiGenerati, int curr, Scheduler scheduler){
        for (int i = 0; i < clientiGenerati.size(); i++) {
            if (clientiGenerati.get(i).getTimpDeSosire() == curr) {
                scheduler.addTask(clientiGenerati.get(i));
                clientiGenerati.remove(i--);
            }
        }
    }

    public static void utilManager2(Scheduler scheduler, FileWriter output, View2 view2) throws IOException {
        for(int i = 0; i < scheduler.getQueues().size(); i ++){
            StringBuilder str = new StringBuilder();
            for(int j = 0; j < scheduler.getQueues().get(i).getNumberOfTasks(); j ++){
                str.append(scheduler.getQueues().get(i).getClients()[j].toString());
            }
            int nr = i + 1;
            output.write("Coada " + nr + ": "+ str);
            output.write("\n");
            view2.setClients(str.toString(), i);
        }
        output.write("\n");
    }

    public static boolean breakCondition(Scheduler scheduler, List<Task> clientiGenerati){
        if(scheduler.empty() == true && clientiGenerati.size() == 0){
            return true;
        }
        return false;
    }
}
