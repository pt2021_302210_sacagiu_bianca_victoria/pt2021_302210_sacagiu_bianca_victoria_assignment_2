import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;

public class View2 extends JFrame {
            private JPanel contentPane;
            private JLabel lbl;
            private JLabel simTime;
            private final ArrayList<JLabel> queuesLabels= new ArrayList<>();
            private JLabel avg;
            private JLabel peekTime;
            private JLabel waitingTime;
            public View2(int numarCozi) {
                setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                setBounds(100, 100, 450, 525);
                contentPane = new JPanel();
                contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
                contentPane.setBackground(new Color(153, 204, 204));
                setFont(new Font("Times New Roman", Font.BOLD, 16));
                setContentPane(contentPane);
                contentPane.setLayout(null);

                JLabel simLbl1 = new JLabel("Simulation");
                simLbl1.setFont(new Font("Times New Roman", Font.PLAIN, 20));
                simLbl1.setBounds(163, 10, 110, 20);
                simLbl1.setForeground(new Color(102, 102, 153));
                simLbl1.setFont(new Font("Times New Roman", Font.BOLD, 20));
                contentPane.add(simLbl1);

                JLabel timeLbl1 = new JLabel("Time:");
                timeLbl1.setFont(new Font("Times New Roman", Font.PLAIN, 15));
                timeLbl1.setBounds(10, 50, 45, 15);
                timeLbl1.setForeground(new Color(102, 102, 153));
                timeLbl1.setFont(new Font("Times New Roman", Font.BOLD, 16));
                contentPane.add(timeLbl1);

                JLabel waitTaskLbl = new JLabel("Waiting tasks:");
                waitTaskLbl.setFont(new Font("Times New Roman", Font.PLAIN, 15));
                waitTaskLbl.setBounds(10, 83, 120, 15);
                waitTaskLbl.setForeground(new Color(102, 102, 153));
                waitTaskLbl.setFont(new Font("Times New Roman", Font.BOLD, 16));
                contentPane.add(waitTaskLbl);

                simTime = new JLabel("");
                simTime.setBounds(55, 50, 45, 15);
                simTime.setForeground(new Color(102, 102, 153));
                simTime.setFont(new Font("Times New Roman", Font.BOLD, 16));
                contentPane.add(simTime);

                lbl = new JLabel("");
                lbl.setBounds(133, 85, 300, 15);
                lbl.setForeground(new Color(102, 102, 153));
                lbl.setFont(new Font("Times New Roman", Font.BOLD, 16));
                contentPane.add(lbl);
                for(int i = 0; i < numarCozi; i ++){
                    JLabel j= new JLabel("Queue"+(i + 1)+": ");
                    j.setFont(new Font("Times New Roman", Font.PLAIN, 15));
                    j.setBounds(10, 120 + i * 30, 80, 20);
                    j.setForeground(new Color(102, 102, 153));
                    j.setFont(new Font("Times New Roman", Font.BOLD, 16));
                    contentPane.add(j);
                    queuesLabels.add(new JLabel(""));
                    queuesLabels.get(i).setFont(new Font("Times New Roman", Font.BOLD, 16));
                    queuesLabels.get(i).setBounds(85, 120 + i * 30, 350, 20);
                    queuesLabels.get(i).setForeground(new Color(102, 102, 153));
                    queuesLabels.get(i).setFont(new Font("Times New Roman", Font.BOLD, 16));
                    contentPane.add(queuesLabels.get(i));
                }
                avg = new JLabel("");
                avg.setBounds(10, 400, 200, 20);
                avg.setForeground(new Color(102, 102, 153));
                avg.setFont(new Font("Times New Roman", Font.BOLD, 16));
                contentPane.add(avg);
                peekTime = new JLabel("");
                peekTime.setBounds(10, 430, 200, 20);
                peekTime.setForeground(new Color(102, 102, 153));
                peekTime.setFont(new Font("Times New Roman", Font.BOLD, 16));
                contentPane.add(peekTime);
                waitingTime = new JLabel("");
                waitingTime.setBounds(10, 460, 200, 20);
                waitingTime.setFont(new Font("Times New Roman", Font.BOLD, 16));
                waitingTime.setForeground(new Color(102, 102, 153));
                contentPane.add(waitingTime);
            }

    public void setSimTime(String s) {
        this.simTime.setText(s);
    }
    public void setWaitClients(String s){
                this.lbl.setText(s);
    }
    public void setClients(String a, int i){
        queuesLabels.get(i).setText(a);
    }
    public void setTimes(String s1, String s2, String s3){
         this.avg.setText(s1);
         this.waitingTime.setText(s2);
         this.peekTime.setText(s3);
    }
}

