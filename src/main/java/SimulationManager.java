import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class SimulationManager implements  Runnable{
    private int finish;
    private int timpMaximDeProcesare;
    private int timpMinimDeProcesare;
    private int numarDeCozi;
    private int numarDeClienti;
    private int timpMinimDeSosire;
    private int timpMaximDeSosire;
    private float oraDeVarf;
    private float max;
    FileWriter output;
    private SelectionPolicy selectionPolicy = SelectionPolicy.SHORTEST_TIME;
    private Scheduler scheduler;
    private View2 GUI2;
    private List<Task> clientiGenerati = new ArrayList<>();
    private int maxNoOfTasksPerServer = 1000;
    private float avgProcessingTime;
    public SimulationManager(int finish, int timpMaximDeProcesare, int timpMinimDeProcesare, int numarDeCozi, int numarDeClienti, int timpMaximDeSosire, int timpMinimDeSosire){
        this.finish = finish;
        this.timpMaximDeProcesare = timpMaximDeProcesare;
        this.timpMinimDeProcesare = timpMinimDeProcesare;
        this.numarDeCozi = numarDeCozi;
        this.numarDeClienti = numarDeClienti;
        this.timpMaximDeSosire = timpMaximDeSosire;
        this.timpMinimDeSosire = timpMinimDeSosire;
        GUI2 = new View2(numarDeCozi);
        GUI2.setVisible(true);
        scheduler = new Scheduler(numarDeCozi, maxNoOfTasksPerServer);
        this.scheduler.changeStrategy(selectionPolicy);
        ProcessingTasks.generateRandomTasks(this.clientiGenerati, this.numarDeClienti, this.timpMaximDeSosire, this.timpMinimDeSosire,
                this.timpMaximDeProcesare, this.timpMinimDeProcesare);
        calculateAvg();
        max = 0;
    }

    @Override
    public void run(){
        int curr = 0;
        try {
            output = new FileWriter("output.txt");
        while(curr <= finish){
            int add = 0;
            output.write("Timp: " + curr + "\n");
            GUI2.setSimTime(String.valueOf(curr));
            StringBuilder s = new StringBuilder();
            ProcessingTasks.utilManager1(clientiGenerati, curr, scheduler);
            for(Task t: clientiGenerati){
                s.append(t.toString()).append(" ");
            }
            output.write("Clienti in asteptare: " + s + "\n");
            GUI2.setWaitClients(s.toString());
            ProcessingTasks.utilManager2(scheduler, output, GUI2);
            for(int i = 0; i < scheduler.getQueues().size(); i ++) {
                if (scheduler.getQueues().get(i).getNumberOfTasks() != 0) {
                    if (scheduler.getQueues().get(i).getClients()[0].getTimpDeProcesare() != 0) {
                        scheduler.getQueues().get(i).getClients()[0].dec();
                    }
                }
            }
            for(int i = 0; i < scheduler.getQueues().size(); i ++){
                add += scheduler.getQueues().get(i).getTimeOfWaiting();
            }
            if(max < add){
                max = add;
                oraDeVarf = curr;
            }
           if(ProcessingTasks.breakCondition(scheduler, clientiGenerati) == true){
               break;
           }
            try {
                Thread.sleep(1050);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            curr++;
        }
        output.write("Timp de procesare mediu: " + avgProcessingTime);
        output.write("\n");
        output.write("Timp de asteptare: " + Server.getSum() / numarDeClienti);
        output.write("\n");
        output.write("Ora de varf: " + oraDeVarf);
        output.write("\n");
        GUI2.setTimes("Avg processing time: : " + avgProcessingTime, "Waiting time: " + Server.getSum() / numarDeClienti, "Peek hour: " + oraDeVarf);
        output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void calculateAvg(){
        float suma = 0;
        for(int i = 0; i < clientiGenerati.size(); i ++){
            suma += clientiGenerati.get(i).getTimpDeProcesare();
            this.avgProcessingTime = suma/ clientiGenerati.size();
        }
    }
}
