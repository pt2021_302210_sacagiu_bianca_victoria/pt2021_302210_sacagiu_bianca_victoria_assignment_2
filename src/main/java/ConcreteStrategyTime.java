import java.util.List;

public class ConcreteStrategyTime implements Strategy {

    @Override
    public void addTask(List<Server> servers, Task t) {
        Server rd = MinArray.computeMinimumTime(servers);
        MinArray.met(rd, t);
    }
}
