import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Server implements Runnable{
    private BlockingQueue<Task> clients;
    private AtomicInteger timeOfWaiting = new AtomicInteger();
    private static float sum;

    public Server(int maxNoOfTask){
        clients = new ArrayBlockingQueue<>(maxNoOfTask);
    }

    public void adaugareTask(Task newTask){
        ProcessingTasks.add(clients, newTask, timeOfWaiting);
    }
    public int getNumberOfTasks(){
        return clients.size();
    }
    public int getTimeOfWaiting() {
        return timeOfWaiting.get();
    }
    public static float getSum() {
        return sum;
    }

    public Task[] getClients() {
        Task[] array = new Task[clients.size()];
        clients.toArray(array);
        return array;
    }

    public  void run(){
        for(;;) {
            if (clients.isEmpty() == false) {
                int timp = clients.peek().getTimpDeProcesare();
                assert clients.peek() != null;
                sum = sum + clients.peek().getTimpDeAsteptare();
            try {
                Thread.sleep(timp * 1050);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            clients.poll();
            timeOfWaiting.set(timeOfWaiting.get() - timp);
            }
        }
    }
}
