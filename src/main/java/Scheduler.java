import java.util.ArrayList;
import java.util.List;

public class Scheduler {
    private List<Server> queues;
    private int nrMaxOfServers;
    private int maxTasksPerServer;
    private Strategy strategy;

    public Scheduler(int nrMaxOfServers, int nrMaxOfTasksPerServer) {
        this.nrMaxOfServers = nrMaxOfServers;
        this.maxTasksPerServer = nrMaxOfTasksPerServer;
        this.queues = new ArrayList<>();
        this.threadStart();
    }

    public void changeStrategy(SelectionPolicy policy) {
        if (policy == SelectionPolicy.SHORTEST_QUEUE) {
            strategy = new ConcreteStrategyQueue();
        }
        if (policy == SelectionPolicy.SHORTEST_TIME) {
            strategy = new ConcreteStrategyTime();
        }
    }

    public void addTask(Task t) {
        strategy.addTask(queues, t);
    }

    public ArrayList<Server> getQueues() {
        return (ArrayList<Server>) queues;
    }

    public boolean empty() {
        for (int i = 0; i < queues.size(); i++) {
            if (queues.get(i).getNumberOfTasks() != 0) {
                return false;
            }
        }
        return true;
    }

    public void threadStart(){
        for (int i = 0; i < this.nrMaxOfServers; i++) {
            this.queues.add(new Server(this.maxTasksPerServer));
            Thread t = new Thread(this.queues.get(i));
            t.start();
        }
    }
}
