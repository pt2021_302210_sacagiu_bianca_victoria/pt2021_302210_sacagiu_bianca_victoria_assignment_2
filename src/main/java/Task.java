public class Task {
    private int timpDeSosire;
    private int timpDeProcesare;
    private int ID;
    private float timpDeAsteptare;
    private static int contor = 0;
    public Task(int timpDeSosire, int timpDeProcesare){
        this.timpDeSosire = timpDeSosire;
        this.timpDeProcesare = timpDeProcesare;
        this.contor++;
        this.ID = this.contor;
    }

    public int getTimpDeSosire() {
        return timpDeSosire;
    }
    public int getTimpDeProcesare() {
        return timpDeProcesare;
    }
    public void dec(){
        this.timpDeProcesare--;
    }
    public String toString(){
        return "(" + this.ID + ", " + this.timpDeSosire + ", " + this.timpDeProcesare + ")";
    }
    public float getTimpDeAsteptare() {
        return timpDeAsteptare;
    }
    public void setTimpDeAsteptare(float timpDeAsteptare) {
        this.timpDeAsteptare = timpDeAsteptare;
    }
}
