import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainClass {
    public static void main(String[] args){
        View GUI= new View();
        GUI.setVisible(true);
        GUI.getSBtn().addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SimulationManager simulationManager = new SimulationManager(Integer.parseInt(GUI.getSimTime()), Integer.parseInt(GUI.getMaxServiceTime()),
                        Integer.parseInt(GUI.getMinServiceTime()), Integer.parseInt(GUI.getNrOfQueues()), Integer.parseInt(GUI.getNrOfClients()),
                        Integer.parseInt(GUI.getMaxArrivalTime()), Integer.parseInt(GUI.getMinArrivalTime()));
                GUI.setVisible(false);
                Thread t = new Thread(simulationManager);
                t.start();
            }
        });
    }
}
