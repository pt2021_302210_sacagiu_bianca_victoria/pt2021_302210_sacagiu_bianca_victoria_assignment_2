import java.util.List;

public class MinArray {

    public static Server computeMinimum(List<Server> sx){
        int min = Integer.MAX_VALUE;
        Server aux = null;
        for(int i = 0; i < sx.size(); i ++){
            if(sx.get(i).getNumberOfTasks() < min) {
                min = sx.get(i).getNumberOfTasks();
                aux = sx.get(i);
            }
        }
        for(int i = 0; i < sx.size(); i ++){
            if(sx.get(i) == aux){
                return sx.get(i);
            }
        }
        return null;
    }

    public static Server computeMinimumTime(List<Server> sx){
        int min = Integer.MAX_VALUE;
        Server aux = null;
        for(int i = 0; i < sx.size(); i ++){
            if(sx.get(i).getTimeOfWaiting() < min){
                min = sx.get(i).getTimeOfWaiting();
                aux = sx.get(i);
            }
        }
        for(int i = 0; i < sx.size(); i ++){
            if(sx.get(i) == aux){
                return sx.get(i);
            }
        }
        return null;
    }

    public static void met(Server rd, Task t){
        int sum = 0;
        for(int i = 0; i < rd.getNumberOfTasks(); i ++){
            sum += rd.getClients()[i].getTimpDeProcesare();
        }
        t.setTimpDeAsteptare(sum + t.getTimpDeProcesare());
        rd.adaugareTask(t);
    }
}
